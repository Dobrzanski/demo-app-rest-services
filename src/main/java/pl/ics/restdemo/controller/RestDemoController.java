package pl.ics.restdemo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.ics.restdemo.model.ApplicationResponse;
import pl.ics.restdemo.model.ExampleObject;
import pl.ics.restdemo.model.ObjectDetails;
import pl.ics.restdemo.service.RestDemoService;

@RestController
public class RestDemoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestDemoController.class);
    private static final String AUTHORIZATION_TOKEN = "Basic QWxhZGRpbjpPcGVuU2VzYW1l";

    @Autowired
    private RestDemoService restDemoService;

    /**
     * Metoda 'POST' odpowiada za dodanie nowego obiektu (nie ma nadanego id) do bazy danych,
     * który przekazywany jest w body (json) zapytania
     * @param newExampleObject
     * @return - HttpStatus.CREATED (201) - gdy obiekt został poprawnie dodany do bazy
     * - HttpStatus.INTERNAL_SERVER_ERROR (500) - gdy brak połączenia z bazą danych
     * - HttpStatus.UNAUTHORIZED (401) - gdy brak w zapytaniu poprawnego tokenu autoryzacji
     *
     * @RequestBody
     * {
     *   "name": "object3",
     *   "priority": 3,
     *   "details": {
     *     "firstDetail": "example detail",
     *     "secondDetail": "second example"
     *   }
     * }
     */
    @PostMapping(value = "/object/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity addNewObject(@RequestBody ExampleObject newExampleObject,
                                       @RequestHeader(value = "Authorization") String authorization) {
        if (!authorization.equals(AUTHORIZATION_TOKEN)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(new ApplicationResponse(HttpStatus.UNAUTHORIZED.value(),
                            HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                            "Required valid Authorization token"));
        }
        return restDemoService.addNewExampleObject(newExampleObject);
    }

    /**
     * Metoda 'GET' zwraca listę wszystkich obiektów przechowywanych w bazie danych
     * @return - HttpStatus.FOUND (302) - gdy poprawnie znaleziono obiekty w bazie
     * - HttpStatus.NOT_FOUND (404) - gdy brak obiektów w bazie danych
     * - HttpStatus.INTERNAL_SERVER_ERROR (500) - gdy brak połączenia z bazą danych
     * - HttpStatus.UNAUTHORIZED (401) - gdy brak w zapytaniu poprawnego tokenu autoryzacji
     */
    @GetMapping(value = "/object/get/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllObjectsData(@RequestHeader(value = "Authorization") String authorization) {
        if (!authorization.equals(AUTHORIZATION_TOKEN)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(new ApplicationResponse(HttpStatus.UNAUTHORIZED.value(),
                            HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                            "Required valid Authorization token"));
        }
        return restDemoService.getAllExampleObjects();
    }

    /**
     * Metoda 'GET' ma za zadanie zwrócić informacje o obiekcie na podstawie jego identyfikatora
     *
     * @param id - identyfikator obiektu, o którym chcemy pobrać informacje
     * @return - HttpStatus.FOUND (302) - gdy poprawnie znaleziono obiekt po identyfikatorze
     * - HttpStatus.BAD_REQUEST (400) - gdy identyfikator obiektu ma niepoprawną formę (np nie jest liczbą)
     * - HttpStatus.NOT_FOUND (404) - gdy obiekt o takim identyfikatorze nie istnieje
     * - HttpStatus.INTERNAL_SERVER_ERROR (500) - gdy brak połączenia z bazą danych
     * - HttpStatus.UNAUTHORIZED (401) - gdy brak w zapytaniu poprawnego tokenu autoryzacji
     */
    @GetMapping(value = "/object/get/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getObjectData(@PathVariable(value = "id", required = true) String id,
                                        @RequestHeader(value = "Authorization") String authorization) {
        if (!authorization.equals(AUTHORIZATION_TOKEN)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(new ApplicationResponse(HttpStatus.UNAUTHORIZED.value(),
                            HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                            "Required valid Authorization token"));
        }
        return restDemoService.getExampleObject(id);
    }

    /**
     * Metoda 'PUT' wykorzystywana jest do aktualizacji obiektu o podanym identyfikaorze.
     * W przypadku aktualizacji zasobu, podmieniany jest cały obiekt.
     * @param id - identyfikator obiektu, który chcemy zaaktualizować
     * @param newExampleObject - ciało obiektu, którym chcemy nadpisać obecny obiekt
     * @return - HttpStatus.OK (200) - gdy poprawnie dokonano aktualizacji zasobu
     * - HttpStatus.BAD_REQUEST (400) - gdy identyfikator obiektu ma niepoprawną formę (np nie jest liczbą)
     * - HttpStatus.NOT_FOUND (404) - gdy obiekt o takim identyfikatorze nie istnieje
     * - HttpStatus.INTERNAL_SERVER_ERROR (500) - gdy brak połączenia z bazą danych
     * - HttpStatus.UNAUTHORIZED (401) - gdy brak w zapytaniu poprawnego tokenu autoryzacji
     */
    @PutMapping(value = "/object/edit/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity editObject(@PathVariable(value = "id", required = true) String id,
                                     @RequestBody ExampleObject newExampleObject,
                                     @RequestHeader(value = "Authorization") String authorization) {

        if (!authorization.equals(AUTHORIZATION_TOKEN)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(new ApplicationResponse(HttpStatus.UNAUTHORIZED.value(),
                            HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                            "Required valid Authorization token"));
        }
        return restDemoService.overrideExampleObject(id, newExampleObject);
    }


    /**
     * Metoda 'PATCH' wykorzystywana jest do aktualizacji wyłącznie pewnej częśći zasobu.
     * @param id - identyfikator obiektu, którego część zasobu ma zostać zaaktualizowana
     * @param objectDetails - aktualizowana część zasobu - szczegóły przykładowego zasobu
     * @return - HttpStatus.OK (200) - gdy poprawnie dokonano aktualizacji CZĘŚCi zasobu
     * - HttpStatus.BAD_REQUEST (400) - gdy identyfikator obiektu ma niepoprawną formę (np nie jest liczbą)
     * - HttpStatus.NOT_FOUND (404) - gdy obiekt o takim identyfikatorze nie istnieje
     * - HttpStatus.INTERNAL_SERVER_ERROR (500) - gdy brak połączenia z bazą danych
     * - HttpStatus.UNAUTHORIZED (401) - gdy brak w zapytaniu poprawnego tokenu autoryzacji
     *
     * * @RequestBody
     * {
     *   "firstDetail": "example detail",
     *   "secondDetail": "second example"
     * }
     */
    @PatchMapping(value = "/object/edit/details/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity editSpecifiedPartOfObject(@PathVariable(value = "id", required = true) String id,
                                                    @RequestBody ObjectDetails objectDetails,
                                                    @RequestHeader(value = "Authorization") String authorization) {

        if (!authorization.equals(AUTHORIZATION_TOKEN)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(new ApplicationResponse(HttpStatus.UNAUTHORIZED.value(),
                            HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                            "Required valid Authorization token"));
        }
        return restDemoService.changeObjectDetails(id, objectDetails);
    }


    /**
     * Metoda 'DELETE' wykorzystujemy do usuwania zasobów
     * @param id - identyfikator obiektu, który chcemy usunąć
     * @return - HttpStatus.OK (200) - gdy poprawnie usunięto obiekt
     * - HttpStatus.BAD_REQUEST (400) - gdy identyfikator obiektu ma niepoprawną formę (np nie jest liczbą)
     * - HttpStatus.NOT_FOUND (404) - gdy obiekt o takim identyfikatorze nie istnieje
     * - HttpStatus.INTERNAL_SERVER_ERROR (500) - gdy brak połączenia z bazą danych
     * - HttpStatus.UNAUTHORIZED (401) - gdy brak w zapytaniu poprawnego tokenu autoryzacji
     */
    @DeleteMapping(value = "/object/delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deleteObject(@PathVariable(value = "id", required = true) String id,
                                       @RequestHeader(value = "Authorization") String authorization) {
        if (!authorization.equals(AUTHORIZATION_TOKEN)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(new ApplicationResponse(HttpStatus.UNAUTHORIZED.value(),
                            HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                            "Required valid Authorization token"));
        }
        return restDemoService.deleteExampleObject(id);
    }
}
