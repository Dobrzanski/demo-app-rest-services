package pl.ics.restdemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.ics.restdemo.model.ExampleObject;

public interface RestDemoRepository extends JpaRepository<ExampleObject, Long> {
}
