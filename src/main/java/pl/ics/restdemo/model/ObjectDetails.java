package pl.ics.restdemo.model;

import javax.persistence.*;

@Entity
@Table(name = "OBJECT_DETAILS")
public class ObjectDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "first_detail", nullable = false, length = 50)
    private String firstDetail;

    @Column(name = "second_detail", nullable = false, length = 50)
    private String secondDetail;

    public ObjectDetails() {
    }

    public ObjectDetails(String firstDetail, String secondDetail) {
        this.firstDetail = firstDetail;
        this.secondDetail = secondDetail;
    }

    @Override
    public String toString() {
        return "ObjectDetails{" +
                "id=" + id +
                ", firstDetail='" + firstDetail + '\'' +
                ", secondDetail='" + secondDetail + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstDetail() {
        return firstDetail;
    }

    public void setFirstDetail(String firstDetail) {
        this.firstDetail = firstDetail;
    }

    public String getSecondDetail() {
        return secondDetail;
    }

    public void setSecondDetail(String secondDetail) {
        this.secondDetail = secondDetail;
    }
}
