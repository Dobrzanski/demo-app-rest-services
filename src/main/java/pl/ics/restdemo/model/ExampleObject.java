package pl.ics.restdemo.model;

import javax.persistence.*;

@Entity
@Table(name = "EXAMPLE_OBJECT")
public class ExampleObject {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(name = "priority", nullable = false, length = 5)
    private int priority;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ObjectDetails details;

    public ExampleObject() {
    }

    public ExampleObject(String name, int priority, ObjectDetails details) {
        this.name = name;
        this.priority = priority;
        this.details = details;
    }

    @Override
    public String toString() {
        return "ExampleObject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", priority=" + priority +
                ", details=" + details +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public ObjectDetails getDetails() {
        return details;
    }

    public void setDetails(ObjectDetails details) {
        this.details = details;
    }
}
