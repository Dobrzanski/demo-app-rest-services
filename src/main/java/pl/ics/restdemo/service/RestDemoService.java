package pl.ics.restdemo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;
import pl.ics.restdemo.dao.RestDemoRepository;
import pl.ics.restdemo.model.ApplicationResponse;
import pl.ics.restdemo.model.ExampleObject;
import pl.ics.restdemo.model.ObjectDetails;

import java.util.List;
import java.util.Optional;

@Service
public class RestDemoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestDemoService.class);

    @Autowired
    private RestDemoRepository restDemoRepository;

    public ResponseEntity getExampleObject(String objectId) {

        Optional<ExampleObject> optionalExampleObject;

        try {
            optionalExampleObject = restDemoRepository.findById(Long.valueOf((objectId)));
        } catch (NumberFormatException nfe) {
            LOGGER.error("Given '" + objectId + "' (" + objectId.getClass().getName() + ") was not correct type of id (required java.lang.Long)");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).
                    body(new ApplicationResponse(HttpStatus.BAD_REQUEST.value(),
                            HttpStatus.BAD_REQUEST.getReasonPhrase(),
                            "Given '" + objectId + "' (" + objectId.getClass().getName() + ") was not correct type of id (required java.lang.Long)"));
        } catch (CannotCreateTransactionException ccte) {
            LOGGER.error("Cannot connect to database");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApplicationResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                            "Cannot connect to database"));
        }

        return optionalExampleObject.<ResponseEntity>map(
                exampleObject -> new ResponseEntity<>(exampleObject, HttpStatus.FOUND))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new ApplicationResponse(HttpStatus.NOT_FOUND.value(),
                                HttpStatus.NOT_FOUND.getReasonPhrase(),
                                "Failed to found 'ExampleObject' with given id: " + objectId)));
    }

    public ResponseEntity getAllExampleObjects() {

        List<ExampleObject> exampleObjects;

        try {
            exampleObjects = restDemoRepository.findAll();
        } catch (CannotCreateTransactionException ccte) {
            LOGGER.error("Cannot connect to database");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApplicationResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                            "Cannot connect to database"));
        }

        return exampleObjects.size() > 0 ?
                new ResponseEntity<List<ExampleObject>>(exampleObjects, HttpStatus.OK) :
                ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new ApplicationResponse(HttpStatus.NOT_FOUND.value(),
                                HttpStatus.NOT_FOUND.getReasonPhrase(),
                                "Can't find any 'ExampleObjects' in database"));
    }

    public ResponseEntity addNewExampleObject(ExampleObject newExampleObject) {

        try {
            restDemoRepository.save(newExampleObject);
        } catch (CannotCreateTransactionException ccte) {
            LOGGER.error("Cannot connect to database");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApplicationResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                            "Cannot connect to database"));
        }

        return new ResponseEntity<ExampleObject>(newExampleObject, HttpStatus.CREATED);
    }

    public ResponseEntity overrideExampleObject(String id, ExampleObject newExampleObject) {
        ExampleObject exampleObjectToOverride = (ExampleObject) getExampleObject(id).getBody();
        exampleObjectToOverride.setName(newExampleObject.getName());
        exampleObjectToOverride.setPriority(newExampleObject.getPriority());

        try {
            restDemoRepository.save(exampleObjectToOverride);
        } catch (CannotCreateTransactionException ccte) {
            LOGGER.error("Cannot connect to database");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApplicationResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                            "Cannot connect to database"));
        }

        return new ResponseEntity<ExampleObject>(exampleObjectToOverride, HttpStatus.OK);
    }

    public ResponseEntity deleteExampleObject(String id) {
        ExampleObject exampleObjectToDelete = (ExampleObject) getExampleObject(id).getBody();

        try {
            restDemoRepository.delete(exampleObjectToDelete);
        } catch (CannotCreateTransactionException ccte) {
            LOGGER.error("Cannot connect to database");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApplicationResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                            "Cannot connect to database"));
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ApplicationResponse(HttpStatus.OK.value(),
                        HttpStatus.OK.getReasonPhrase(),
                        "Successfully delete 'ExampleObject' with given id: " + id));
    }

    public ResponseEntity changeObjectDetails(String id, ObjectDetails objectDetails) {
        ExampleObject exampleObjectTChangeDetails = (ExampleObject) getExampleObject(id).getBody();
        exampleObjectTChangeDetails.setDetails(objectDetails);

        try {
            restDemoRepository.save(exampleObjectTChangeDetails);
        } catch (CannotCreateTransactionException ccte) {
            LOGGER.error("Cannot connect to database");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApplicationResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                            "Cannot connect to database"));
        }

        return new ResponseEntity<ExampleObject>(exampleObjectTChangeDetails, HttpStatus.OK);
    }
}
