Api Documentation (full version after running application: http://localhost:8080/rest-demo/swagger-ui.html)
=================

### /object/add
---
##### ***POST***
**Summary:** addNewObject

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Authorization | header | Authorization | Yes | string |
| newExampleObject | body | newExampleObject | Yes | [ExampleObject](#exampleobject) |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Created | [ResponseEntity](#responseentity) |
| 401 | Unauthorized |  |
| 500 | Internal Server Error |  |

### /object/delete/{id}
---
##### ***DELETE***
**Summary:** deleteObject

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Authorization | header | Authorization | Yes | string |
| id | path | id | Yes | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK |  |
| 400 | Bad Request |  |
| 401 | Unauthorized |  |
| 404 | Not Found |  |
| 500 | Internal Server Error |  |

### /object/edit/details/{id}
---
##### ***PATCH***
**Summary:** editSpecifiedPartOfObject

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Authorization | header | Authorization | Yes | string |
| id | path | id | Yes | string |
| objectDetails | body | objectDetails | Yes | [ObjectDetails](#objectdetails) |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ResponseEntity](#responseentity) |
| 400 | Bad Request |  |
| 401 | Unauthorized |  |
| 404 | Not Found |  |
| 500 | Internal Server Error |  |

### /object/edit/{id}
---
##### ***PUT***
**Summary:** editObject

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Authorization | header | Authorization | Yes | string |
| id | path | id | Yes | string |
| newExampleObject | body | newExampleObject | Yes | [ExampleObject](#exampleobject) |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ResponseEntity](#responseentity) |
| 400 | Bad Request |  |
| 401 | Unauthorized |  |
| 404 | Not Found |  |
| 500 | Internal Server Error |  |

### /object/get/all
---
##### ***GET***
**Summary:** getAllObjectsData

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Authorization | header | Authorization | Yes | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 302 | Found | [ResponseEntity](#responseentity) |
| 401 | Unauthorized |  |
| 404 | Not Found |  |
| 500 | Internal Server Error |  |

### /object/get/{id}
---
##### ***GET***
**Summary:** getObjectData

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Authorization | header | Authorization | Yes | string |
| id | path | id | Yes | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | OK | [ResponseEntity](#responseentity) |
| 400 | Bad Request |  |
| 401 | Unauthorized |  |
| 404 | Not Found |  |
| 500 | Internal Server Error |  |

### Models
---

### ExampleObject  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| details | [ObjectDetails](#objectdetails) |  | No |
| id | long |  | No |
| name | string |  | No |
| priority | integer |  | No |

### ObjectDetails  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| firstDetail | string |  | No |
| id | long |  | No |
| secondDetail | string |  | No |

### ResponseEntity  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| body | object |  | No |
| statusCode | string |  | No |
| statusCodeValue | integer |  | No |